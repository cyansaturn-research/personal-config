require('packer').startup(function()
 
  -- Packer manages itself
  use 'wbthomason/packer.nvim'

  -- Themes
  use 'Mofiqul/dracula.nvim'
  use 'drewtempelmeyer/palenight.vim'

  -- Color Highlighter
  use 'norcalli/nvim-colorizer.lua'

  -- Lualine
  use {
    'hoob3rt/lualine.nvim',
    requires = {'kyazdani42/nvim-web-devicons', opt = true}
  }

  -- LSP
  use 'neovim/nvim-lspconfig'
  use 'kabouzeid/nvim-lspinstall'
  use 'glepnir/lspsaga.nvim'

  -- Treesitter
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }

  -- Telescope
  use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
  }

end)

-- Set colorscheme
vim.cmd[[set termguicolors]]
vim.cmd[[colorscheme palenight]]

-- Enabling colorizer
require('colorizer').setup()

-- Lualine Setup
require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'palenight',
    component_separators = {'|'},
    section_separators = {},
  }
}
vim.cmd[[set noshowmode]]
