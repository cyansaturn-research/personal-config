vim.bo.expandtab = true
vim.bo.shiftwidth = 2
vim.bo.softtabstop = 2
vim.wo.number = true
vim.wo.relativenumber = true
vim.bo.syntax = "on"

require('_keymaps')
require('_plugins')
require('_lspconfig')
require('_telescope')
